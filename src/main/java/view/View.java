package view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class View {
    protected static Logger logger = LogManager.getLogger(View.class);

    public void showMessage(String message) {
        logger.info(message);
    }

    public abstract void start();

    public abstract void showMenu();
}
